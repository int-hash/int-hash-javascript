class Hasher {
    constructor(
        alphabet = 'cvNHxsw0AZXthfrYUJqaVRLPnCkiol75MKImQuTGB84Ee93ySWpDgb2OjFdz16'
    ) {
        this.alphabet = alphabet
    }
    
    hash(intValue, size) {
        let rest
        let hash = ''
        let quotient = intValue

        do {
            rest = quotient % this.alphabet.length
            hash = this.alphabet[rest] + hash
            quotient = (quotient - rest) / this.alphabet.length
            rest = quotient
        } while (quotient > 0)

        const zerocount = size - hash.length;
        for (let i = 0; i < zerocount; i++) {
            hash = this.alphabet[0] + hash
        }
        return hash
    }
}

console.log(new Hasher().hash(1000036, 6))